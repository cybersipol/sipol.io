﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.IO.Abstractions;

namespace Sipol.IO.Cache.Tests.Unit
{
    [TestFixture()]
    public class FileCacheChecker_Tests
    {
        private FileCacheChecker sut;

        [SetUp]
        public void Init()
        {
            sut = new FileCacheChecker();
            FS.Current = new SipolFileSys(true);
        }


        #region IsInCache Tests

        [Test()]
        public void IsInCache_CheckExistsCall_FileExistsCalled()
        {
            FS.File = Substitute.For<FileBase>();

            string path = @"c:\test.txt";

            sut.IsInCache(path);

            FS.File.Received().Exists(path);
        }


        [Test()]
        public void IsInCache_ExistsFile_ReturnTrue()
        {
            FS.File = Substitute.For<FileBase>();

            string path = @"c:\test.txt";
            FS.File.Exists(path).Returns(true);

            bool result = sut.IsInCache(path);

            Assert.That(result, 
                        Is.True
                        );
        }


        [Test()]
        public void IsInCache_NotExistsFile_ReturnFalse()
        {
            FS.File = Substitute.For<FileBase>();

            string path = @"c:\test.txt";
            FS.File.Exists(path).Returns(false);

            bool result = sut.IsInCache(path);

            Assert.That(result,
                        Is.False
                        );
        }


        [Test()]
        public void IsInCache_NullName_ReturnFalseAndNotReceiveExists()
        {
            FS.File = Substitute.For<FileBase>();
            bool result = sut.IsInCache(null);

            Assert.That(result,
                        Is.False
                        );
            FS.File.DidNotReceive().Exists(Arg.Any<string>());
        }

        [Test()]
        public void IsInCache_EmptyName_ReturnFalseAndNotReceiveExists()
        {
            FS.File = Substitute.For<FileBase>();
            bool result = sut.IsInCache(String.Empty);

            Assert.That(result,
                        Is.False
                        );
            FS.File.DidNotReceive().Exists(Arg.Any<string>());
        }

        [Test()]
        public void IsInCache_WhitespaceName_ReturnFalseAndNotReceiveExists()
        {
            FS.File = Substitute.For<FileBase>();
            bool result = sut.IsInCache("   ");

            Assert.That(result,
                        Is.False
                        );

            FS.File.DidNotReceive().Exists(Arg.Any<string>());
        }

        #endregion
    }
}