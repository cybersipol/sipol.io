﻿using NUnit.Framework;
using Sipol.IO.Dirs;
using Sipol.IO.Dirs.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using System.IO.Abstractions;

namespace Sipol.IO.Dirs.Tests.Unit
{
    [TestFixture()]
    public class WorkingSubDir_Tests
    {
        private WorkingSubDir sut = null;
        private IWorkingDir workingDirMock = null;
        private string subDirName1 = null;
        private string defaultWorkingDirPath = null;

        [SetUp]
        public void Init()
        {
            sut = null;
            workingDirMock = null;
            defaultWorkingDirPath = @"c:\WorkinSubDir_Tests";
            subDirName1 = "test";
            FS.Current = new SipolFileSys(settersOn: true);
            FS.Current.Directory = Substitute.For<DirectoryBase>();
            FS.Current.Directory.Exists(Arg.Any<string>()).Returns(true);
        }

        #region ctor Tests

        [Test()]
        public void ctor_NullWorkinDir_ThrowException()
        {
            Assert.That(() => 
                            {
                                object result = new WorkingSubDir(null, "blablabla");
                            }, 
                            Throws.ArgumentNullException
            );

        }
         

        [TestCase(null)]
        [TestCase("")]
        [TestCase("    ")]
        public void ctor_subDirNameEmptyOrNullOrWhieSpace_ThrowException(string subDirName) 
        {
            workingDirMock = Substitute.For<IWorkingDir>();
            Assert.That(() =>
                            {
                                object result = new WorkingSubDir(workingDirMock, subDirName);
                            },
                            Throws.ArgumentNullException
            );


        }


        [TestCase("aaa\"aaa")]
        [TestCase("*")]
        [TestCase("?")]
        [TestCase("\"")]
        [TestCase("b*b*?>")]
        [TestCase("<")]
        [TestCase("<tag>")]
        [TestCase("<tag/>")]
        public void ctor_WrongCharInSubDirName_ThrowException(string subDirName)
        {
            workingDirMock = Substitute.For<IWorkingDir>();
            Assert.That(() =>
                            {
                                object result = new WorkingSubDir(workingDirMock, subDirName);
                            },
                            Throws.ArgumentException
            );
        }


        [TestCase(@"c:/other/sub/dir")]
        [TestCase(@"c:\other\sub/dir")]
        public void ctor_SubDirIsRooted_ThrowException(string subDirName)
        {
            workingDirMock = Substitute.For<IWorkingDir>();
                                Assert.That(() =>
                                {
                                    object result = new WorkingSubDir(workingDirMock, subDirName);
                                },
                                Throws.ArgumentException
            );
        }


        [TestCase("some_sub_dir")]
        [TestCase(@"other\sub\dir")]
        [TestCase(@"another/sub/dir")]
        public void ctor(string subDirName)
        {
            workingDirMock = Substitute.For<IWorkingDir>();
            workingDirMock.Path.Returns(@"C:\temp");
            object result = new WorkingSubDir(workingDirMock, subDirName);

            Assert.That(result,
                    Is.Not.Null &
                    Is.InstanceOf<WorkingSubDir>()
                );

            Console.WriteLine((result as WorkingSubDir).Path);

        }


        [Test()]
        public void ctor_NotExistDir_CreateDir()
        {
            FS.Current.Directory = Substitute.For<DirectoryBase>();
            FS.Current.Directory.Exists(Arg.Any<string>()).Returns(false);
            sut = CreateSUT();

            FS.Current.Directory.Received().CreateDirectory(Arg.Any<string>());
        }

        [Test()]
        public void ctor_AtachToWorkingDir_AttachObservatorIsCalled()
        {
            sut = CreateSUT();

            workingDirMock.Received().AttachObservator(sut);
        }

        
        #endregion


        #region IsInObservators Tests

        [Test()]
        public void IsInObservators_byDefault_IsInList()
        {
            AttachObservator_byDefault_IsInListOfObservators();
        }


        [Test()]
        public void IsInObservators_Null_ReturnFalse()
        {
            sut = CreateSUT();
        
            Assert.That(sut.IsInObservators(null),
                      Is.False);

        }

        #endregion

        
        #region AttachObservator Tests

        [Test()]
        public void AttachObservator_NullObservator_NotNullInListOfObservators()
        {
            sut = CreateSUT();
            IWorkingDirObservator obs1 = null;
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);

            Assert.That(sut.IsInObservators(obs1),
                      Is.False);

            Assert.That(sut.IsInObservators(obs2),
                      Is.True);
        }
         

        [Test()]
        public void AttachObservator_byDefault_IsInListOfObservators()
        {
            sut = CreateSUT();
            IWorkingDirObservator obs1 = CreateWorkingDirObservatorMock();
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);

            Assert.That(sut.IsInObservators(obs1), 
                      Is.True);

            Assert.That(sut.IsInObservators(obs2),
                      Is.True);
        }

        #endregion


        #region DetachObservator Tests

        [Test()]
        public void DetachObservator_NullObservator_NoDetachObservatorsFromList()
        {
            sut = CreateSUT();
            IWorkingDirObservator obs1 = CreateWorkingDirObservatorMock();
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);

            sut.DetachObservator(null);
            sut.DetachObservator(obs2);


            Assert.That(sut.IsInObservators(obs1),
                      Is.True);

            Assert.That(sut.IsInObservators(obs2),
                      Is.False);
        }


        [Test()]
        public void DetachObservator_ByDefault_DetachObservatorsFromList()
        {
            sut = CreateSUT();
            IWorkingDirObservator obs1 = CreateWorkingDirObservatorMock();
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);

            sut.DetachObservator(obs1);
            sut.DetachObservator(obs2);


            Assert.That(sut.IsInObservators(obs1),
                      Is.False);

            Assert.That(sut.IsInObservators(obs2),
                      Is.False);
        }

        #endregion


        #region DetachAllObservators Tests

        [Test()]
        public void DetachAllObservators_byDefault_NoAttachedObservatorsOnList()
        {
            sut = CreateSUT();
            IWorkingDirObservator obs1 = CreateWorkingDirObservatorMock();
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);

            sut.DetachAllObservators();

            Assert.That(sut.IsInObservators(obs1),
                      Is.False);

            Assert.That(sut.IsInObservators(obs2),
                      Is.False);
        }

        #endregion


        #region UpdateWorkingDirectory Tests

        [Test()]
        public void UpdateWorkingDirectory_NullWorkingDir_NoChange()
        {
            sut = CreateSUT();

            string subDirName = sut.SubDirectoryName;
            string path = sut.Path;
            string nSubDirName = nameof(sut.SubDirectoryName);
            string nPath = nameof(sut.Path);

            //Console.WriteLine("{0} / {1}", nPath, nSubDirName);

            sut.UpdateWorkingDirectory(null);

            Assert.That(sut,
                        Is.Not.Null &
                        Has.Property(nPath).EqualTo(path) &
                        Has.Property(nSubDirName).EqualTo(subDirName)
                        );
        }

        [Test()]
        public void UpdateWorkingDirectory_NoSameWorkingDir_NoChange()
        {
            sut = CreateSUT();

            string subDirName = sut.SubDirectoryName;
            string path = sut.Path;
            string nSubDirName = nameof(sut.SubDirectoryName);
            string nPath = nameof(sut.Path);

            IWorkingDir dir = Substitute.For<IWorkingDir>();
            dir.Path.Returns(@"Z:\yyauauau");

            sut.UpdateWorkingDirectory(dir);

            Assert.That(sut,
                        Is.Not.Null &
                        Has.Property(nPath).EqualTo(path) &
                        Has.Property(nSubDirName).EqualTo(subDirName)
                        );
        }


        [Test()]
        public void UpdateWorkingDirectory_ChangeParentWorkingDir_ChangeFullPathAndNotyfiktyObservators()
        {
            string dir = @"Z:\newTestFolder";
            string dir2 = @"Z:\newTestFolder2";
            string subDirName = "subdir";
            WorkingDir wdir = new WorkingDir(dir);

            sut = CreateSUT(wdir, subDirName);
            IWorkingDirObservator obs1 = CreateWorkingDirObservatorMock();
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);

            wdir.Path = dir2;

            Assert.That(sut.Path,
                        Is.EqualTo(dir2 + FS.Current.Path.DirectorySeparatorChar + subDirName)
                        );

            Assert.That(sut.SubDirectoryName,
                        Is.EqualTo(subDirName)
                        );

            obs1.Received().UpdateWorkingDirectory(sut);
            obs2.Received().UpdateWorkingDirectory(sut);
        }

        #endregion

        #region PathAndSubDirName Tests

        [TestCase("newTestSubFolder")] 
        [TestCase(@"newTestSubFolder\ccc")] 
        [TestCase("newTestSubFolder/aaa/bbb")] 
        public void PathAndSubDirName_ChangePath_ChangeSubDirName(string newPath)
        {
            sut = CreateSUT();
            sut.Path = newPath;

            Assert.That(sut.SubDirectoryName,
                    Is.Not.Null &
                    Is.Not.Empty &
                    Is.EqualTo(newPath)
                    );
        }


        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void PathAndSubDirName_ChangePathToNullOrEmpty_ThrowException(string newPath)
        {
            sut = CreateSUT();

            Assert.That(() =>
            {
                sut.Path = newPath;
            },
            Throws.ArgumentNullException
            );

        }


        [TestCase(@"D:\aaa")]
        [TestCase(@"C:\eee\test")]
        [TestCase(@"C:")]
        public void PathAndSubDirName_ChangePathToRooted_ThrowException(string newPath)
        {
            sut = CreateSUT();

            Assert.That(() =>
            {
                sut.Path = newPath;
            },
            Throws.ArgumentException
            );

        }


        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void PathAndSubDirName_ChangeSubDirNamrToNullOrEmpty_ThrowException(string newPath)
        {
            sut = CreateSUT();

            Assert.That(() =>
            {
                sut.Path = newPath;
            },
            Throws.ArgumentNullException
            );

        }


        [TestCase(@"c:\test\sub\dir")]
        [TestCase(@"c:\test\aaaa")]
        public void PathAndSubDirName_ChangeSubDirNameToRootedInWorkinDir_ChangeSubDir(string subDirName)
        {
            workingDirMock = Substitute.For<IWorkingDir>();
            workingDirMock.Path.Returns(@"c:\test");
            sut = CreateSUT(workingDirMock, "zzz");

            sut.SubDirectoryName = subDirName;

            Assert.That(sut.Path,
                        Is.Not.Null &
                        Contains.Substring(@"c:\test\")
                        );

            Assert.That(sut.SubDirectoryName,
                        Is.Not.Null &
                        Is.EqualTo(subDirName.Replace(@"c:\test\", ""))
                        );
        }


        [TestCase(@"D:\aaa")]
        [TestCase(@"C:\eee\test")]
        [TestCase(@"C:")]
        public void PathAndSubDirName_ChangeSubDirNameToRooted_ThrowException(string subDirName)
        {
            sut = CreateSUT();

            Assert.That(() =>
            {
                sut.SubDirectoryName = subDirName;
            },
            Throws.ArgumentException
            );

        }


        [TestCase("newTestSubFolder")]
        [TestCase(@"newTestSubFolder\ccc")]
        [TestCase("newTestSubFolder/aaa/bbb")]
        public void PathAndSubDirName_ChangeSubDirName_ChangePath(string newPath)
        {
           
            sut = CreateSUT();
            
            sut.SubDirectoryName = newPath;

            Console.WriteLine(sut.Path);
            Console.WriteLine(sut.SubDirectoryName);

            Assert.That(sut.Path,
                    Is.Not.Null &
                    Is.Not.Empty &
                    Is.EqualTo(defaultWorkingDirPath + FS.Current.Path.DirectorySeparatorChar + newPath)
                    );
        }


        #endregion


        #region NotifyObservators Tests 

        [Test()]
        public void NotifyObservators_ChangePath_UpdateWorkingDirectoryIsCalledOnObservators()
        {
            sut = CreateSUT();
            IWorkingDirObservator obs1 = CreateWorkingDirObservatorMock();
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);
            sut.Path = @"tatatata";

            obs1.Received().UpdateWorkingDirectory(sut);
            obs2.Received().UpdateWorkingDirectory(sut);
        }


        [Test()]
        public void NotifyObservators_ChangeSubDirName_UpdateWorkingDirectoryIsCalledOnObservators()
        {
            sut = CreateSUT();
            IWorkingDirObservator obs1 = CreateWorkingDirObservatorMock();
            IWorkingDirObservator obs2 = CreateWorkingDirObservatorMock();
            sut.AttachObservator(obs1);
            sut.AttachObservator(obs2);
            sut.SubDirectoryName = @"tatatata";

            obs1.Received().UpdateWorkingDirectory(sut);
            obs2.Received().UpdateWorkingDirectory(sut);
        }

        #endregion




        #region Help funcs

        private WorkingSubDir CreateSUT(IWorkingDir workingDir, string subDirName)
        {
            sut = new WorkingSubDir(workingDir, subDirName);
            return sut;
        }


        private WorkingSubDir CreateSUT(string subDirName)
        {
            workingDirMock = CreateWorkingDirMock();
            workingDirMock.Path.Returns(defaultWorkingDirPath);
            return CreateSUT(workingDirMock, subDirName);
        }


        private WorkingSubDir CreateSUT()
        {
            return CreateSUT(subDirName1);
        }


        private IWorkingDir CreateWorkingDirMock()
        {
            workingDirMock = Substitute.For<IWorkingDir>();
            return workingDirMock;
        }


        private IWorkingDirObservator CreateWorkingDirObservatorMock()
        {
            return Substitute.For<IWorkingDirObservator>();
        }


        #endregion
    }
}