﻿using NSubstitute;
using NUnit.Framework;
using Sipol.IO.Net.Interfaces;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Sipol.IO.Files.Tests.Unit
{
    [TestFixture()]
    public class UrlContentLoader_Tests
    {
        UrlContentLoaderSUT sut = null;

        [SetUp]
        public void Init()
        {
            sut = null;
        }


        #region ctor Tests

        [Test()]
        public void ctor_NullUrl_ThrowException()
        {
            Assert.That(() =>
                {
                    object result = new UrlContentLoader(null);
                },
                Throws.ArgumentNullException
                );
        }


        [Test()]
        public void ctor_UrlWithFileName_PathHasFileName()
        {
            Uri url = new Uri("http://test.org/default/schemas/test.xsd");
            sut = new UrlContentLoaderSUT(url);
            Assert.That(sut.Path,
                        Is.Not.Null &
                        Is.Not.Empty &
                        Is.EqualTo("test.xsd")
                        );
        }


        [Test()]
        public void ctor_UrlWithoutFileName_PathEmpty()
        {
            Uri url = new Uri("http://test.org/default/schemas/");
            sut = new UrlContentLoaderSUT(url);
            Assert.That(sut.Path,
                        Is.Not.Null &
                        Is.Empty
                        );
        }


        [Test()]
        public void ctor_UrlWithFileNameWithoutExtension_PathHasFileNameWithoutExtension()
        {
            Uri url = new Uri("http://test.org/default/schemas/test");
            sut = new UrlContentLoaderSUT(url);
            Assert.That(sut.Path,
                        Is.Not.Null &
                        Is.Not.Empty &
                        Is.EqualTo("test")
                        );
        }


        [Test()]
        public void ctor_UrlWithParams_PathHasFileNameWithoutParams()
        {
            Uri url = new Uri("http://test.org/default/schemas/test.xsd?haha=1&bbb=ttt");
            sut = new UrlContentLoaderSUT(url);
            Assert.That(sut.Path,
                        Is.Not.Null &
                        Is.Not.Empty &
                        Is.EqualTo("test.xsd")
                        );
        }


        [Test()]
        public void ctor()
        {
            Uri url = new Uri("http://test.org/default/schemas/test.xsd");
            object result = new UrlContentLoader(url);
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<UrlContentLoader>()
                        );
        }

        #endregion


        #region GetFileContent

        [Test()]
        public void GetFileContent_byDefault_ReadUrlContent()
        {
            byte[] expectedContent = Encoding.UTF8.GetBytes("some text");
            sut = new UrlContentLoaderSUT(new Uri("http://test.org/default/schemas/test.xsd?d=1"));

            sut.WebDataLoader = Substitute.For<IWebDataLoader>();
            sut.WebDataLoader.DownloadData(null).ReturnsForAnyArgs(expectedContent);

            byte[] result = sut.GetFileContent();

            sut.WebDataLoader.ReceivedWithAnyArgs().DownloadData(null);

            Assert.That(result,
                        Is.Not.Null &
                        Is.EquivalentTo(expectedContent)
                        );
        }


        [Test()]
        public void GetFileContent_NetError_ThrowException()
        {
            sut = new UrlContentLoaderSUT(new Uri("http://test.org/default/schemas/test.xsd?d=1"));

            sut.WebDataLoader = Substitute.For<IWebDataLoader>();
            sut.WebDataLoader.DownloadData(null).ReturnsForAnyArgs(x => { throw new WebException("test"); });

            Assert.That(() =>
            {
                byte[] result = sut.GetFileContent();
            },
            Throws.InstanceOf<WebException>().With.Message.EqualTo("test")
            );
        }


        #endregion


        #region Help classes

        class UrlContentLoaderSUT : UrlContentLoader
        {
            public UrlContentLoaderSUT(Uri url) : base(url)
            {
            }

            public Stream ContentStream { get => SwapStream; set { SwapStream = value; } }
        }


        #endregion

        [Test()]
        public void Dispose_byDefault_IsWebDataLoaderDisposeMethodIsCalled()
        {
            sut = new UrlContentLoaderSUT(new Uri("http://test.org/default/schemas/test.xsd?d=1"));
            sut.WebDataLoader = Substitute.For<IWebDataLoader>();

            sut.Dispose();

            sut.WebDataLoader.ReceivedWithAnyArgs().Dispose();
        }
    }
}