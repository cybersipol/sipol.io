﻿using NSubstitute;

using NUnit.Framework;

using Sipol.IO.Net.Interfaces;
using System;
using System.Collections.Specialized;

namespace Sipol.IO.Net.Tests.Unit
{
    [TestFixture()]
    public class WebDataSender_Tests
    {
        private WebDataSender sut;
        private IWebClient webClient;
        private Uri uri;

        [SetUp]
        public void Init()
        {
            webClient = null;
            uri = new Uri("http://test.org/test.html");
            sut = null;
        }


        #region ctor Tests


        [Test()]
        public void ctor()
        {
            // Arrange

            // Act
            object result = new WebDataSender(CreateMockWebClient(), uri, "ble");

            // Assert
            Assert.That(result,
                            Is.Not.Null &
                            Is.InstanceOf<WebDataSender>()
            );
        }




        [Test()]
        public void ctor__WhenWebClientIsNull__ThrowArgNullException()
        {
            // Arrange
            // Act
            // Assert
            Assert.That(() =>
                        {
                            object result = new WebDataSender(null, uri, "ble");
                        },
                        Throws.ArgumentNullException
            );

        }

        [Test()]
        public void ctor__WhenUriIsNull__ThrowArgNullException()
        {
            // Arrange
            // Act
            // Assert
            Assert.That(() =>
            {
                object result = new WebDataSender(CreateMockWebClient(), null, "ble");
            },
                        Throws.ArgumentNullException
            );

        }
        #endregion


        #region SendWebData Tests


        [Test()]
        public void SendWebData__byDefault__IsCallUploadStringOnWebClient()
        {
            // Arrange
            string expected = "some web data";

            webClient = CreateMockWebClient();
            sut = CreateSUT(webClient, uri, expected);

            // Act
            string result = sut.SendWebData();

            // Assert
            webClient.Received(1).UploadString(uri,"POST", expected);
        }


        [Test()]
        public void SendWebData__byDefault__ResultEqualToUploadStringResult()
        {
            // Arrange
            webClient = CreateMockWebClient();
            string data = "some web data";
            
            sut = CreateSUT(webClient, uri, data);

            string expectedResult = "some result";

            webClient.UploadString(uri, "POST", data).Returns(expectedResult);

            // Act
            string result = sut.SendWebData();

            // Assert
            Assert.That(result,
                            Is.EqualTo(expectedResult)
            );
        }



        [Test()]
        public void SendWebData__WhenWebClientThrowException__ThrowThisSameException()
        {
            // Arrange
            webClient = CreateMockWebClient();
            Exception exception = new Exception();

            string data = "some web data";
            sut = CreateSUT(webClient, uri, data);

            webClient.UploadString(uri, "POST", data).Returns(p => throw exception );

            // Act
            // Assert
            Assert.That(() =>
                        {
                            string result = sut.SendWebData();
                        }
                        , Throws.Exception.SameAs(exception)
            );
        }


        #endregion


        #region Help funcs

        private WebDataSender CreateSUT( IWebClient _webClient, Uri url, object data = null )
        {
            sut = new WebDataSender(_webClient, url, data);
            return sut;
        }

        private WebDataSender CreateSUT() => CreateSUT(webClient, uri);


        private IWebClient CreateMockWebClient()
        {
            webClient = Substitute.For<IWebClient>();
            return webClient;            
        }



        #endregion
    }
}