﻿using NSubstitute;

using NUnit.Framework;

using Sipol.IO.Dirs;
using Sipol.IO.Dirs.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;

namespace Sipol.IO.Unit.Tests
{
    [TestFixture]
    public class DirecoryFilesLister_Tests
    {
        private DirectoryFilesLister sut;
        private IDirectoryInfo dir;
        private List<string> filenames;

        [SetUp]
        public void Init()
        {
            FS.Current = new SipolFileSys(true);
            FS.Directory = Substitute.For<IDirectory>();
            FS.DirectoryInfo = Substitute.For<IDirectoryInfoFactory>();
            
            FS.File = Substitute.For<IFile>();
            dir = Substitute.For<IDirectoryInfo>();

            sut = null;
        }


        #region ctor Tests


        [Test()]
        public void ctor()
        {
            // Arrange
            string path = @"Z:\test\dir";
            CreateDefaultDirWithFiles(path);

            // Act
            var result = new DirectoryFilesLister(path);


            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(result,
                                Is.InstanceOf<DirectoryFilesLister>()
                            );

                Assert.That(result,
                                Is.Not.Null
                            );

            }
            );
        }


        [Test()]
        public void ctor__WithWorkinDir()
        {
            // Arrange
            IWorkingDir dir = Substitute.For<IWorkingDir>();
            string path = @"Z:\test\dir";
            dir.Path.Returns(path);
            CreateDefaultDirWithFiles(path);

            // Act
            var result = new DirectoryFilesLister(dir);


            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(result,
                                Is.InstanceOf<DirectoryFilesLister>()
                            );

                Assert.That(result,
                                Is.Not.Null
                            );

            }
            );
        }

        [Test()]
        public void ctor__WhenWorkinDirIsNull__ThrowArgumentNullException()
        {
            // Arrange
            IWorkingDir dir = null;

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = new DirectoryFilesLister(dir);
                },
                Throws.ArgumentNullException
            );
        }


        [Test()]
        public void ctor__WhenDirectoryNotExists__ThrowDirectoryNotFoundException()
        {
            // Arrange
            string path = @"Z:\test\dir";
            FS.Directory.Exists(path).Returns(false);
            FS.File.GetAttributes(path).Returns(FileAttributes.Directory);

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = new DirectoryFilesLister(path);
                },
                Throws.InstanceOf<DirectoryNotFoundException>()
            );
        }


        [Test()]
        public void ctor__WhenPathIsNotDirectory__ThrowExceptionWithMessage()
        {
            // Arrange
            string path = @"Z:\test\dir";
            FS.Directory.Exists(path).Returns(true);
            FS.File.GetAttributes(path).Returns(FileAttributes.Normal);

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = new DirectoryFilesLister(path);
                },
                Throws.Exception.With.Message.Contain("Path is not directory")
            );
        }


        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void ctor__WhenPathIsNullOrWhitespace__ThrowArgumentNullException(string path)
        {
            // Arrange
            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = new DirectoryFilesLister(path);
                },
                Throws.ArgumentNullException
            );
        }

        #endregion


        #region ReadFiles Tests


        [Test()]
        public void ReadFiles__byDefault__ResultIsListOfPathsOfFiles()
        {
            // Arrange
            string path = @"Z:\test\dir";
            CreateDefaultDirWithFiles(path);

            sut = CreateSUT(path);

            // Act
            var result = sut.ReadFiles();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(result,
                            Is.Not.Null
                );

                Assert.That(result,
                            Is.Not.Empty);

                Assert.That(result,
                            Is.EquivalentTo(filenames)
                );
            }
            );
        }


        [Test()]
        public void ReadFiles__byDefault__PropertyFilesHasListOfPathsOfFilesInDir()
        {
            // Arrange
            string path = @"Z:\test\dir";
            CreateDefaultDirWithFiles(path);

            sut = CreateSUT(path);

            // Act
            var result = sut.ReadFiles();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(sut.Files,
                            Is.Not.Null
                );

                Assert.That(sut.Files,
                            Is.Not.Empty);

                Assert.That(sut.Files,
                            Is.EquivalentTo(filenames)
                );

                Assert.That(sut.Files,
                            Is.SameAs(result)
                );
            }
            );

            
        }


        [Test()]
        public void ReadFiles__WhenNoHasFilter__CallGetFilesOnDir()
        {
            // Arrange
            string path = @"Z:\test\dir";
            CreateDefaultDirWithFiles(path);

            sut = CreateSUT(path);

            // Act
            var result = sut.ReadFiles();

            // Assert
            dir.Received(1).GetFiles();
        }


        [Test()]
        public void ReadFiles__WhenHasFilter__CallGetFilesOnDirWithFilter()
        {
            // Arrange
            string path = @"Z:\test\dir";
            string filter = "some filter";
            CreateDefaultDirWithFiles(path, filter);

            sut = CreateSUT(path,filter);

            // Act
            var result = sut.ReadFiles();

            // Assert
            dir.Received(1).GetFiles(filter);
        }


        [Test()]
        public void ReadFiles__byDefault__CallFromDirectoryNameOnFSDirectoryInfo()
        {
            // Arrange
            string path = @"Z:\test\dir";
            CreateDefaultDirWithFiles(path);

            sut = CreateSUT(path);

            // Act
            var result = sut.ReadFiles();

            // Assert
            FS.DirectoryInfo.Received().FromDirectoryName(path);
        }

        #endregion


        #region help funcs

        private DirectoryFilesLister CreateSUT(string path, string filter = null)
        {
            sut = new DirectoryFilesLister(path, filter);
            return sut;
        }

        private void CreateDefaultDirWithFiles( string path, string filter = null )
        {
            FS.Directory.Exists(path).Returns(true);
            FS.File.GetAttributes(path).Returns(FileAttributes.Directory);

            FS.DirectoryInfo.FromDirectoryName(path).Returns(dir);
            dir.FullName.Returns(path);
            IFileInfo f1 = Substitute.For<IFileInfo>();
            IFileInfo f2 = Substitute.For<IFileInfo>();
            filenames = new List<string>() { "file.one", "file.two" };

            f1.FullName.Returns(path + "\\" + filenames[0]);
            f2.FullName.Returns(path + "\\" + filenames[1]);
            f1.Name.Returns(FS.Path.GetFileName(filenames[0]));
            f2.Name.Returns(FS.Path.GetFileName(filenames[1]));

            if (String.IsNullOrWhiteSpace(filter))
                dir.GetFiles().Returns(new IFileInfo[] { f1, f2 });
            else
                dir.GetFiles(filter).Returns(new IFileInfo[] { f1, f2 });

        }

        #endregion

    }
}
