﻿using NSubstitute;

using NUnit.Framework;

using Sipol.IO.Dirs;
using Sipol.IO.Dirs.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;

namespace Sipol.IO.Unit.Tests
{
    [TestFixture]
    public class DirectoryFilesWatcher_Tests
    {
        private DirectoryFilesWatcher sut;
        private IDirectoryFilesLister dirFilesLister;
        private IFileSystemWatcher fsWatcher;
        private string defaultPath = @"Z:\test\directory";
        private string defaultFilter = "*.txt";

        [SetUp]
        public void Init()
        {
            FS.Current = new SipolFileSys(true);
            FS.Directory = Substitute.For<IDirectory>();
            FS.File = Substitute.For<IFile>();

            dirFilesLister = Substitute.For<IDirectoryFilesLister>();

            dirFilesLister.Path.Returns(defaultPath);
            dirFilesLister.Filter.Returns(defaultFilter);

            fsWatcher = Substitute.For<IFileSystemWatcher>();

            sut = null;
        }

        #region ctor Tests

        [Test()]
        public void ctor_WithPath__NotNullAndInstanceOfSUTClass()
        {
            // Arrange
            string path = @"Z:\test\dir";

            FS.Directory.Exists(path).Returns(true);
            FS.File.GetAttributes(path).Returns(FileAttributes.Directory);

            // Act
            var result = new DirectoryFilesWatcher(path, null, fsWatcher);

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<DirectoryFilesWatcher>()
            );

        }

        [Test()]
        public void ctor_WithPath__ResultHasSameFsWatcherAsInConstructor()
        {
            // Arrange
            string path = @"Z:\test\dir";

            FS.Directory.Exists(path).Returns(true);
            FS.File.GetAttributes(path).Returns(FileAttributes.Directory);

            // Act
            var result = new DirectoryFilesWatcher(path, null, fsWatcher);

            // Assert
            Assert.That(result?.FsWatcher,
                            Is.Not.Null &
                            Is.SameAs(fsWatcher)
            );
        }


        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void ctor_WithPath__WhenPathIsIncorrectString__ThrowArgumentNullExcpetion(string path)
        {
            // Arrange

            FS.Directory.Exists(path).Returns(x => throw new ArgumentException());
            FS.File.GetAttributes(path).Returns(x => throw new ArgumentException());
             
            // Act
            // Arrange
            Assert.That(() =>
                {
                    var result = new DirectoryFilesWatcher(path, null, fsWatcher);
                },
                Throws.ArgumentNullException
            );
        }

        [Test()]
        public void ctor_WithDirLister__ResultIsNotNullAndInstaceOfSutClass()
        {
            // Arrange
            string path = @"Z:\test\dir";

            FS.Directory.Exists(path).Returns(true);
            FS.File.GetAttributes(path).Returns(FileAttributes.Directory);

            IDirectoryFilesLister directoryFilesLister = Substitute.For<IDirectoryFilesLister>();
            directoryFilesLister.Path.Returns(path);

            // Act
            var result = new DirectoryFilesWatcher(directoryFilesLister, fsWatcher);

            // Assert
            Assert.That(result,
                            Is.Not.Null &
                            Is.InstanceOf<DirectoryFilesWatcher>()
            );
        }


        [Test()]
        public void ctor_WithDirLister__ResultHasSameFsWatcherAsInConstructor()
        {
            // Arrange
            string path = @"Z:\test\dir";

            FS.Directory.Exists(path).Returns(true);
            FS.File.GetAttributes(path).Returns(FileAttributes.Directory);

            IDirectoryFilesLister directoryFilesLister = Substitute.For<IDirectoryFilesLister>();
            directoryFilesLister.Path.Returns(path);

            // Act
            var result = new DirectoryFilesWatcher(directoryFilesLister,  fsWatcher);

            // Assert
            Assert.That(result?.FsWatcher,
                            Is.Not.Null &
                            Is.SameAs(fsWatcher)
            );
        }


        [Test()]
        public void ctor_WithDirLister__WhenDirListerIsNull__ResultIsNotNullAndInstaceOfSutClass()
        {
            // Arrange
            IDirectoryFilesLister directoryFilesLister = null;

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = new DirectoryFilesWatcher(directoryFilesLister, fsWatcher);
                },
                Throws.ArgumentNullException
            );
        }


        [Test()]
        public void ctor_WithWorkingDir__ResultIsNotNullAndInstaceOfSutClass()
        {
            // Arrange
            string path = @"Z:\test\dir";

            FS.Directory.Exists(path).Returns(true);
            FS.File.GetAttributes(path).Returns(FileAttributes.Directory);

            IWorkingDir workingDir = Substitute.For<IWorkingDir>();
            workingDir.Path.Returns(path);
            
            // Act
            var result = new DirectoryFilesWatcher(workingDir, null, fsWatcher);

            // Assert
            Assert.That(result,
                            Is.Not.Null &
                            Is.InstanceOf<DirectoryFilesWatcher>()
            );
        }


        [Test()]
        public void ctor_WithWorkingDir__WhenWorkingDirIsNull__ThrowArgumentNullException()
        {
            // Arrange
            IWorkingDir workingDir = null;

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = new DirectoryFilesWatcher(workingDir, null, fsWatcher);
                },
                Throws.ArgumentNullException
            );
        }

        #endregion


        #region WatchOn Tests

        [Test()]
        public void WatchOn__byDefault__CallReadFilesOnDirFilesLister()
        {
            // Arrange
            sut = CreateSUT(dirFilesLister, fsWatcher);
            // Act

            sut.WatchOn();

            // Assert
            dirFilesLister.Received().ReadFiles();
        }


        [Test()]
        public void WatchOn__byDefault__FilesNotEmptyAndEquivalentToPathsFromReadFiles()
        {
            // Arrange
            List<string> paths = new List<string>() { defaultPath + FS.SeparatorChar.Directory + "file_one.txt", defaultPath + FS.SeparatorChar.Directory + "file_two.txt" };

            dirFilesLister.ReadFiles().Returns(paths);
            sut = CreateSUT(dirFilesLister, fsWatcher);
            // Act
            sut.WatchOn();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(sut.Files,
                                Is.Not.Null
                            );

                Assert.That(sut.Files,
                                Is.Not.Empty
                            );

                Assert.That(sut.Files,
                                Is.EquivalentTo(paths)
                            );

            }
            );

        }

        [Test()]
        public void WatchOn__byDefault__FsWatcherEnableRaisingEventsSetToTrue()
        {
            // Arrange
            sut = CreateSUT(dirFilesLister, fsWatcher);
            // Act
            sut.WatchOn();

            // Assert
            fsWatcher.Received().EnableRaisingEvents = true;            
        }


        [Test()]
        public void WatchOn__WhenReadFilesReturnNull__NoThrowExceptionAndFilesIsNotNullButEmpty()
        {
            // Arrange
            List<string> paths = null;

            dirFilesLister.ReadFiles().Returns(paths);
            sut = CreateSUT(dirFilesLister, fsWatcher);
            // Act
            sut.WatchOn();

            // Assert
            Assert.That(() =>
                {
                    sut.WatchOn();
                },
                Throws.Nothing
            );

            Assert.Multiple(() =>
            {
                Assert.That(sut.Files,
                                Is.Not.Null
                            );

                Assert.That(sut.Files,
                                Is.Empty
                            );

            }
            );

        }

        #endregion


        #region WatchOff Tests


        [Test()]
        public void WatchOff__byDefault__SetFsWatcherEnableRaisingEventsSetToFalse()
        {
            // Arrange
            sut = CreateSUT(dirFilesLister, fsWatcher);

            // Act
            sut.WatchOff();

            // Assert
            fsWatcher.Received().EnableRaisingEvents = false;
        }

        #endregion


        #region Event_AddFileToWatchedDir Tests

        [Test()]
        public void Event_AddFileToWatchedDir__byDefault__EventWasRaised()
        {
            // Arrange
            sut = CreateSUT(dirFilesLister, fsWatcher);

            bool wasCreated = false;

            fsWatcher.Created += ( sender, args ) => wasCreated = true;

            sut.WatchOn();
            // Act
            fsWatcher.Created += Raise.Event<FileSystemEventHandler>(this, new FileSystemEventArgs(WatcherChangeTypes.Created, defaultPath, "test01.txt"));

            // Assert
            Assert.That(wasCreated,
                        Is.True);
        }

        [Test()]
        public void Event_AddFileToWatchedDir__byDefault__AddFileToFiles()
        {
            // Arrange
            sut = CreateSUT(dirFilesLister, fsWatcher);

            string filename = "test01.txt";

            sut.WatchOn();
            // Act
            fsWatcher.Created += Raise.Event<FileSystemEventHandler>(this, new FileSystemEventArgs(WatcherChangeTypes.Created, defaultPath, filename));

            // Assert
            Assert.That(sut.Files,
                            Contains.Item(filename)
            );
        }

        #endregion


        #region Event_DeleteFileFromWatchedDir Tests

        [Test()]
        public void Event_DeleteFileFromWatchedDir__byDefault__EventWasraisedAndFilesNoHasDeletedPath()
        {
            // Arrange
            List<string> paths = new List<string>() { "file_one.txt", "file_two.txt" };
            string pathToDelete = paths[0];

            dirFilesLister.ReadFiles().Returns(paths);
            sut = CreateSUT(dirFilesLister, fsWatcher);
            bool wasRaised = false;

            fsWatcher.Deleted += ( sender, args ) => wasRaised = true;

            sut.WatchOn();

            // Act
            fsWatcher.Deleted += Raise.Event<FileSystemEventHandler>(this, new FileSystemEventArgs(WatcherChangeTypes.Deleted, defaultPath, FS.Path.GetFileName(pathToDelete)));

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(wasRaised,
                                Is.True
                );

                Assert.That(sut.Files,
                            Does.Not.Contains(pathToDelete)
                );
            }
            );

        }


        #endregion


        #region Event_ChangeFileFromWatchedDir Tests


        [Test()]
        public void Event_ChangeFileFromWatchedDir__byDefault__FileOnTheEnd()
        {
            // Arrange
            List<string> paths = new List<string>() { "file_one.txt", "file_two.txt" };
            int indexChange = 0;
            string pathToChange = paths[indexChange];

            dirFilesLister.ReadFiles().Returns(paths);
            sut = CreateSUT(dirFilesLister, fsWatcher);
            bool wasRaised = false;

            fsWatcher.Changed += ( sender, args ) => wasRaised = true;

            sut.WatchOn();

            // Act
            fsWatcher.Changed += Raise.Event<FileSystemEventHandler>(this, new FileSystemEventArgs(WatcherChangeTypes.Deleted, defaultPath, FS.Path.GetFileName(pathToChange)));

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(wasRaised,
                                Is.True
                );

                Assert.That(sut.Files[indexChange],
                                Is.Not.EqualTo(pathToChange)
                            
                );

                Assert.That(sut.Files[sut.Files.Count-1],
                                Is.EqualTo(pathToChange)

                );
            }
            );
        }


        #endregion


        #region Event_RenameFileInWatchedDir Tests


        [Test()]
        public void Event_RenameFileInWatchedDir__byDefault__RemoveOldFilePathAndAddNewFilePath()
        {
            // Arrange
            List<string> paths = new List<string>() { "file_one.txt", "file_two.txt" };
            int indexChange = 0;
            string pathToChange = paths[indexChange];
            string newname = "renamed_" + FS.Path.GetFileName(pathToChange);
            dirFilesLister.ReadFiles().Returns(paths);
            sut = CreateSUT(dirFilesLister, fsWatcher);
            bool wasRaised = false;

            fsWatcher.Renamed += ( sender, args ) => wasRaised = true;

            sut.WatchOn();

            // Act
            fsWatcher.Renamed += Raise.Event<RenamedEventHandler>(this, new RenamedEventArgs(WatcherChangeTypes.Renamed, defaultPath, newname , FS.Path.GetFileName(pathToChange)));

            // Assert
            Assert.That(wasRaised,
                            Is.True
            );

            Assert.That(sut.Files,
                            Does.Not.Contains(pathToChange)

            );

            Assert.That(sut.Files,
                            Does.Contain(newname)

            );

        }

        #endregion

        #region OnFilesListChanged Tests


        [Test()]
        public void OnFilesListChanged__byDefault__InvokeEventOnFilesListChangedForEveryOperationOnFilesInDir()
        {
            // Arrange
            List<string> paths = new List<string>() { defaultPath + FS.SeparatorChar.Directory + "file_one.txt", defaultPath + FS.SeparatorChar.Directory + "file_two.txt" };
            int indexChange = 0;
            string pathToChange = paths[indexChange];
            string pathToDelete = paths[1];
            string newname = "renamed_" + FS.Path.GetFileName(pathToChange);
            dirFilesLister.ReadFiles().Returns(paths);
            sut = CreateSUT(dirFilesLister, fsWatcher);

            int numCalledFunc = 0;

            sut.OnFilesListChanged += (type, name, second) => numCalledFunc++;

            // Act
            fsWatcher.Created += Raise.Event<FileSystemEventHandler>(this, new FileSystemEventArgs(WatcherChangeTypes.Created, defaultPath, "test01.txt"));
            fsWatcher.Deleted += Raise.Event<FileSystemEventHandler>(this, new FileSystemEventArgs(WatcherChangeTypes.Deleted, defaultPath, FS.Path.GetFileName(pathToDelete)));
            fsWatcher.Changed += Raise.Event<FileSystemEventHandler>(this, new FileSystemEventArgs(WatcherChangeTypes.Deleted, defaultPath, FS.Path.GetFileName(pathToChange)));
            fsWatcher.Renamed += Raise.Event<RenamedEventHandler>(this, new RenamedEventArgs(WatcherChangeTypes.Renamed, defaultPath, newname, FS.Path.GetFileName(pathToChange)));


            // Assert
            Assert.That(numCalledFunc,
                            Is.EqualTo(4)
            );
        }


        #endregion


        #region help funcs

        private DirectoryFilesWatcher CreateSUT( IDirectoryFilesLister _directoryFilesLister, IFileSystemWatcher _fileSystemWatcher = null)
        {
            sut = new DirectoryFilesWatcher(_directoryFilesLister, _fileSystemWatcher);
            return sut;
        }

        private DirectoryFilesWatcher CreateSUT( string _path, string _filter = null, IFileSystemWatcher _fileSystemWatcher = null )
        {
            sut = new DirectoryFilesWatcher(_path, _filter, _fileSystemWatcher);
            return sut;

        }

        #endregion

    }
}
