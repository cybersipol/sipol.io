﻿using System;

namespace Sipol.IO.Cache
{
    public class FileCacheCreationTimeChecker : FileCacheChecker
    {
        public FileCacheCreationTimeChecker(TimeSpan maxTime)
        {
            MaxTime = maxTime;
        }

        public TimeSpan MaxTime { get; set; } = TimeSpan.Zero;
        public DateTime TimeEndPoint { get; set; } = DateTime.MinValue;
        

        public override bool IsInCache(string name)
        {
            SetTimeEndPoint();

            if (base.IsInCache(name))
            {
                if (MaxTime <= TimeSpan.Zero)
                    return true;

                DateTime fileTime = FS.File.GetCreationTime(name);

                return (TimeEndPoint - fileTime).Duration() <= MaxTime.Duration();
            }

            return false;
        }

        private void SetTimeEndPoint()
        {
            if (TimeEndPoint == DateTime.MinValue)
                TimeEndPoint = DateTime.Now;
        }
    }
}
