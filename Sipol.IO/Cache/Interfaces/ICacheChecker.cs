﻿namespace Sipol.IO.Cache.Interfaces
{
    public interface ICacheChecker
    {
        bool IsInCache( string name );
    }
}
