﻿using System.IO;
using System;
using Sipol.IO.Dirs.Interfaces;

namespace Sipol.IO.Dirs
{
    public class DirectoryCreator: IDirectoryInformation
    {
        public string Dir { get; private set; } = String.Empty;
        public DirectoryInfo DirectoryInfo => DirectoryInfoExtensions.CreateDirectoryInfo(Dir);
        public bool Exists => IsExists();

        public DirectoryCreator(string dir)
        {
            Dir = dir ?? throw new NullReferenceException("Dir path is NULL (class " + GetType().FullName + ")");
            if (String.IsNullOrWhiteSpace(Dir)) throw new NullReferenceException("Dir path is EMPTY (class " + GetType().FullName + ")");
        }

        public void CreateDirectoryPath()
        {
            if (IsExists()) return;

            DirectoryInfo parent = DirectoryInfo.Parent;
            if (parent != null)
            {
                DirectoryCreator creatorDir = new DirectoryCreator(parent.FullName);
                creatorDir.CreateDirectoryPath();
            }

            DirectoryInfo.Create();
        }

        public bool IsExists()
        {
            return DirectoryInfo.Exists;
        }

        public static DirectoryCreator Create(string dir)
        {
            return new DirectoryCreator(dir);
        }


    }
}
