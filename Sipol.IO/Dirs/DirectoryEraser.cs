﻿using Sipol.IO.Dirs.Interfaces;
using System;
using System.IO;

namespace Sipol.IO.Dirs
{
    public class DirectoryEraser: IDirectoryInformation
    {
        public string Dir { get; private set; } = null;

        public DirectoryInfo DirectoryInfo => DirectoryInfoExtensions.CreateDirectoryInfo(Dir);

        public bool Exists => DirectoryInfo.Exists;

        public DirectoryEraser(string dir)
        {
            Dir = dir ?? throw new NullReferenceException("Dir is NULL (class " + GetType().FullName + ")");
        }

        public void Erase()
        {
            if (!Exists) return;

            EraseAllFilesInDirectory();

            EraseAllDirsInDirectory();

            DirectoryInfo.Delete();
        }

        private void EraseAllFilesInDirectory()
        {
            FileInfo[] files = DirectoryInfo.GetFiles();
            if (files == null) return;
            if (files.Length <= 0) return;
            foreach (FileInfo file in files)
            {
                file.Refresh();
                if (file.Exists) file.Delete();
            }
        }

        private void EraseAllDirsInDirectory()
        {
            DirectoryInfo[] dirs = DirectoryInfo.GetDirectories();
            if (dirs == null) return;
            if (dirs.Length<=0) return;
            foreach (DirectoryInfo dir in dirs)
            {
                dir.Refresh();
                
                DirectoryEraser eraser = new DirectoryEraser(dir.FullName);
                eraser.Erase();
            }
        }

        public static DirectoryEraser Create(string dir)
        {
            return new DirectoryEraser(dir);
        }


    }
}
