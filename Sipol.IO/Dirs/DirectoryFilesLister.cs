﻿using Sipol.IO.Dirs.Interfaces;

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;

namespace Sipol.IO.Dirs
{
    public class DirectoryFilesLister : IDirectoryFilesLister
    {
        public string Path { get; private set; } = String.Empty;
        public string Filter { get; private set; } = String.Empty;
        public List<String> Files { get; private set; } = null;
        public int Count => Files?.Count ?? 0;

        public DirectoryFilesLister(string path, string filter = null)
        {
            CheckPathIsDirectory(path);

            IDirectoryInfo dir = FS.DirectoryInfo.FromDirectoryName(path);

            Path = dir.FullName;
            Filter = filter;
            Files = new List<String>();
        }


        public DirectoryFilesLister( IWorkingDir workingDir, string filter = null )
        {
            if (workingDir is null)
                throw new ArgumentNullException(nameof(workingDir));

            string path = workingDir.Path;

            CheckPathIsDirectory(path);

            IDirectoryInfo dir = FS.DirectoryInfo.FromDirectoryName(path);

            Path = dir.FullName;
            Filter = filter;
            Files = new List<String>();
        }



        public List<String> ReadFiles()
        {
            Files.Clear();

            CheckPathIsDirectory(Path);

            IDirectoryInfo dir = FS.DirectoryInfo.FromDirectoryName(Path);
            IFileInfo[] files = (String.IsNullOrWhiteSpace(Filter) ? dir.GetFiles() : dir.GetFiles(Filter));

            Files.AddRange(from file in files
                            orderby file.FullName
                            select file.Name);

            return Files;
        }

        #region static funcs

        public static void CheckPathIsDirectory(String path)
        {
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException(nameof(path));

            if (FS.Directory.Exists(path) == false)
                throw new DirectoryNotFoundException(path);

            FileAttributes attr = FS.File.GetAttributes(path);

            if (attr.HasFlag(FileAttributes.Directory) == false)
                throw new Exception("Path is not directory");


        }



        #endregion

    }
}
