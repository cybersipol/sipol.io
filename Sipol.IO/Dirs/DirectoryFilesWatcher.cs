﻿using Sipol.IO.Dirs.Interfaces;

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;

namespace Sipol.IO.Dirs
{
    public class DirectoryFilesWatcher : IDirectoryFilesWatcher
    {

        public DirectoryFilesWatcher( string path, string filter = null, IFileSystemWatcher fsWatcher = null )
        {
            DirectoryFilesLister = CreateDefaultDirecortyFileLister(path, filter);
            SetFsWatcherDefaults(fsWatcher);
            Files = new List<string>();
        }

        public DirectoryFilesWatcher( IDirectoryFilesLister directoryFilesLister, IFileSystemWatcher fsWatcher = null )
        {
            DirectoryFilesLister = directoryFilesLister ?? throw new ArgumentNullException(nameof(directoryFilesLister));
            SetFsWatcherDefaults(fsWatcher);
            Files = new List<string>();
        }


        public DirectoryFilesWatcher( IWorkingDir workingDir, string filter = null, IFileSystemWatcher fsWatcher = null )
        {
            if (workingDir is null)
                throw new ArgumentNullException(nameof(workingDir));

            string path = workingDir.Path;

            DirectoryFilesLister = CreateDefaultDirecortyFileLister(path, filter);
            SetFsWatcherDefaults(fsWatcher);
            Files = new List<string>();
        }

        public event Action<WatcherChangeTypes, string, string> OnFilesListChanged;


        public string Path => DirectoryFilesLister?.Path ?? String.Empty;
        public string Filter => DirectoryFilesLister?.Filter ?? String.Empty;

        public IFileSystemWatcher FsWatcher { get; private set; }
        public IDirectoryFilesLister DirectoryFilesLister { get; private set; }

        public List<string> Files { get; private set; }
        

        public void WatchOn()
        {
            Files.Clear();
            Files.AddRange(DirectoryFilesLister.ReadFiles() ?? new List<string>());

            FsWatcher.EnableRaisingEvents = true;
        }

        public void WatchOff()
        {
            FsWatcher.EnableRaisingEvents = false;
        }


        #region help funcs

        private IFileSystemWatcher CreateDefaultFileWatcher()
        {
            if (FsWatcher is null)
                FsWatcher = new FileSystemWatcherWrapper(Path);

            return FsWatcher;
        }

        private IDirectoryFilesLister CreateDefaultDirecortyFileLister( string path, string filter = null )
        {
            if (DirectoryFilesLister is null)
                DirectoryFilesLister = new DirectoryFilesLister(path, filter);

            return DirectoryFilesLister;
        }
        

        private void SetFsWatcherDefaults( IFileSystemWatcher fsWatcher = null )
        {
            FsWatcher = fsWatcher ?? CreateDefaultFileWatcher();

            FsWatcher.Path = Path;
            FsWatcher.IncludeSubdirectories = false;
            if (String.IsNullOrWhiteSpace(Filter) == false)
                FsWatcher.Filter = Filter;
            else
                FsWatcher.Filter = "*.*";

            FsWatcher.NotifyFilter = NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.FileName
                                ;

            FsWatcher.Changed -= FsWatcher_Changed;
            FsWatcher.Created -= FsWatcher_Created;
            FsWatcher.Deleted -= FsWatcher_Deleted;
            FsWatcher.Renamed -= FsWatcher_Renamed;


            FsWatcher.Changed += FsWatcher_Changed;
            FsWatcher.Created += FsWatcher_Created;
            FsWatcher.Deleted += FsWatcher_Deleted;
            FsWatcher.Renamed += FsWatcher_Renamed;
        }

        private void FsWatcher_Changed( object sender, FileSystemEventArgs e )
        {
            Files.Remove(e.Name);
            Files.Add(e.Name);
            OnFilesListChanged?.Invoke(e.ChangeType, e.Name, e.FullPath);
        }

        private void FsWatcher_Created( object sender, FileSystemEventArgs e )
        {
            Files.Remove(e.Name);
            Files.Add(e.Name);
            OnFilesListChanged?.Invoke(e.ChangeType, e.Name, e.FullPath);
        }

        private void FsWatcher_Deleted( object sender, FileSystemEventArgs e )
        {
            Files.Remove(e.Name);
            OnFilesListChanged?.Invoke(e.ChangeType, e.Name, e.FullPath);
        }

        private void FsWatcher_Renamed( object sender, RenamedEventArgs e )
        {
            Files.Remove(e.OldName);
            OnFilesListChanged?.Invoke(WatcherChangeTypes.Deleted, e.OldName, e.OldFullPath);

            Files.Add(e.Name);
            OnFilesListChanged?.Invoke(WatcherChangeTypes.Created, e.Name, e.FullPath);
        }

        #endregion

    }
}
