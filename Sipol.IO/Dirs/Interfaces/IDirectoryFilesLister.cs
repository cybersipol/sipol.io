﻿using System.Collections.Generic;
using System.IO.Abstractions;

namespace Sipol.IO.Dirs.Interfaces
{
    public interface IDirectoryFilesLister
    {
        string Path { get; }
        string Filter { get; }


        List<string> ReadFiles();
    }
}