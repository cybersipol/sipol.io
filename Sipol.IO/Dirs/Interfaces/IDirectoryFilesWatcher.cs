﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Sipol.IO.Dirs.Interfaces
{
    public interface IDirectoryFilesWatcher
    {
        event Action<WatcherChangeTypes, string, string> OnFilesListChanged;

        List<string> Files { get; }
        string Path { get; }

        void WatchOff();
        void WatchOn();
    }
}