﻿using System.IO;

namespace Sipol.IO.Dirs.Interfaces
{
    public interface IDirectoryInformation
    {
        string Dir { get; }
        DirectoryInfo DirectoryInfo { get; }
        bool Exists { get; }
    }
}
