﻿namespace Sipol.IO.Dirs.Interfaces
{
    public interface IWorkingDirObservator
    {
        void UpdateWorkingDirectory(IWorkingDir workingDir);
    }
}
