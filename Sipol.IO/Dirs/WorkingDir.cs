﻿using Sipol.IO.Dirs.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace Sipol.IO.Dirs
{
    public class WorkingDir : IWorkingDir, IWorkinDirObservable
    {
        private string _dir         = String.Empty;
        private string _previousDir = String.Empty;

        public string Path
        {
            get => _dir ?? String.Empty;
            set => SetDir(value);
        }

        public DirectoryInfo DirectoryInfo => DirectoryInfoExtensions.CreateDirectoryInfo(Path);

        public string PreviousPath => _previousDir ?? String.Empty;

        public DirectoryInfo PreviousDirectoryInfo => String.IsNullOrWhiteSpace(PreviousPath) ? null : DirectoryInfoExtensions.CreateDirectoryInfo(PreviousPath);

        private List<IWorkingDirObservator> Observators = null;

        public WorkingDir(string dirPath)
        {
            Path = dirPath;
            CheckDir();
        }


        private void SetDir(string value)
        {
            if (String.Equals(value ?? String.Empty, Path)) return;

            _previousDir = _dir;
            _dir = value;

            CheckDir();

            if (!Path.Equals(PreviousPath))
                NotifyObservators();
        }

        protected virtual void CheckDir()
        {
            if (String.IsNullOrWhiteSpace(Path)) throw new NullReferenceException("Dir is EMPTY (class " + GetType().FullName + ")");
            if (!FS.Current.Directory.Exists(Path)) throw new DirectoryNotFoundException("Dir not EXISTS (class " + GetType().FullName + ")");
        }

        public void NotifyObservators()
        {
            if (Observators == null) return;
            if (Observators.Count <= 0) return;

            foreach (IWorkingDirObservator observer in Observators)
            {
                if (observer != null)
                    observer.UpdateWorkingDirectory(this);
            }
        }

        private void CreateObservatorsList()
        {
            if (Observators == null) Observators = new List<IWorkingDirObservator>();
        }

        public bool IsInObservators(IWorkingDirObservator observator)
        {
            if (Observators == null) return false;
            return Observators.IndexOf(observator) >= 0;
        }

        public void AttachObservator(IWorkingDirObservator observator)
        {
            if (observator == null) return;
            CreateObservatorsList();
            if (IsInObservators(observator)) return;
            Observators.Add(observator);
        }

        public void DetachObservator(IWorkingDirObservator observator)
        {
            if (observator == null) return;
            if (!IsInObservators(observator)) return;
            Observators.Remove(observator);
        }

        public void DetachAllObservators()
        {
            if (Observators == null) return;
            Observators.Clear();
        }
       
    }
}
