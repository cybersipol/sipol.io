﻿using Sipol.IO.Files.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Sipol.IO.Files
{
    public class AlternativeFileNames : IAlternativeFileNames
    {
        public string FilePath { get; private set; } = String.Empty;
        public string BaseName { get; private set; } = String.Empty;
        public string Extension { get; private set; } = String.Empty;

        public string Dir => dirInfo?.FullName ?? String.Empty;

        public List<string> SearchFileNames { get; private set; }

        private FileInfo fileInfo { get; set; }
        private DirectoryInfo dirInfo { get; set; }

        public AlternativeFileNames(string filepath)
        {
            FilePath = filepath ?? throw new NullReferenceException("FilePath is NULL (class " + GetType().FullName + ")");
            if (String.IsNullOrWhiteSpace(FilePath)) throw new NullReferenceException("FilePath is EMPTY (class " + GetType().FullName + ")");
            createInfoFile();

            BaseName = Path.GetFileNameWithoutExtension(FilePath);
            Extension = Path.GetExtension(FilePath);

            SearchFileNames = new List<string>();
        }

        private void createInfoFile()
        {
            fileInfo = new FileInfo(FilePath);
            dirInfo = fileInfo.Directory;
        }

        public string SearchAlternativeFilesInDirectory()
        {
            return SearchAlternativeFilesInDirectory(Dir);
        }

        public string SearchAlternativeFilesInDirectory(string dir)
        {
            if (String.IsNullOrWhiteSpace(dir)) return String.Empty;

            DirectoryInfo di = new DirectoryInfo(dir);
            di.Refresh();
            if (!di.Exists) return String.Empty;

            foreach (string filename in SearchFileNames)
            {
                if (String.IsNullOrWhiteSpace(filename)) continue;

                if (filename.Contains("*") || filename.Contains("?"))
                {
                    FileInfo[] fis = di.GetFiles(filename);
                    if (fis!=null && fis.Length>0)
                    {
                        return (from f in fis
                                orderby f.Name
                                select f).First().FullName;
                    }
                }
                else
                {
                    FileInfo fi = new FileInfo(dir + Path.DirectorySeparatorChar + filename);
                    fi.Refresh();
                    if (fi.Exists) return fi.FullName;
                }
            }

            return String.Empty;
        }

        




    }
}
