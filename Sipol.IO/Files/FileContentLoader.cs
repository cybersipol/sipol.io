﻿using Sipol.IO.Files.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Sipol.IO.Files
{
    public class FileContentLoader : IFileContentLoader, ITextFileContentLoader
    {
        public ushort BUFFOR_SIZE { get; set; } = 1024;
        public string Path { get; protected set; } = null;

        protected Stream SwapStream = null;

        private Stream stream = null;
        private byte[] buffor = null;

        #region ctor
        protected FileContentLoader()
        {
            SwapStream = null;
            stream = null;
            buffor = null;
            Path = null;
        }


        public FileContentLoader(string path): this()
        {
            Path = !String.IsNullOrEmpty(path) ? path: throw new ArgumentNullException(nameof(path));
        }

        #endregion


        public virtual byte[] GetFileContent()
        {
            if (!IsFileExists())
                throw new FileNotFoundException(Path);

            stream = GetStream();

            CreateBuffor();
            int numBytesRead = ReadStreamToBuffor();
            List<byte> result = new List<byte>();
            while (numBytesRead > 0)
            {
                result.AddRange(GetBuffor(numBytesRead));
                numBytesRead = ReadStreamToBuffor();
            }

            CloseStream();
            return result.ToArray();
        }


        public virtual string GetFileTextContent(Encoding encoding)
        {
            if (encoding==null)
                throw new ArgumentNullException(nameof(encoding));

            byte[] bytesResult = GetFileContent();
            return encoding.GetString(bytesResult);
        }

        #region Help funcs

        protected virtual bool IsFileExists() => FileExister.Exists(Path);


        private Stream GetStream()
        {
            CloseStream();
            Stream str = new BufferedStream(SwapStream ?? File.OpenRead(Path), (int)BUFFOR_SIZE);
            str.Seek(0, SeekOrigin.Begin);
            str.Position = 0;
            return str;
        }


        private void CreateBuffor()
        {
            buffor = new byte[BUFFOR_SIZE];
        }


        private int ReadStreamToBuffor() => stream.Read(buffor, 0, buffor.Length);


        private IEnumerable<byte> GetBuffor(int numBytes) => buffor.Take(numBytes);


        private void CloseStream()
        {
            if (stream != null)
            {
                stream.Close();
                stream.Dispose();
                stream = null;
            }
        }

        #endregion

    }
}
