﻿using Sipol.IO.Files.Interfaces;

namespace Sipol.IO.Files
{
    public static class FileExister
    {
        private static bool? defaultIsFileExists = null;
        public static IFileExists FileExistsChecker = null;

        public static void Set(bool fileExistsReturn)
        {
            defaultIsFileExists = fileExistsReturn;
        }

        public static void Reset()
        {
            defaultIsFileExists = null;
        }

        public static bool Exists(string path)
        {
            if (defaultIsFileExists == null)
                return (GetFileExistsChecker().IsExists(path));

            return (defaultIsFileExists == true ? true : false);
        }

        public static IFileExists GetFileExistsChecker()
        {
            if (FileExistsChecker==null) FileExistsChecker = new FileExists();
            return FileExistsChecker;
        }

    }
}
