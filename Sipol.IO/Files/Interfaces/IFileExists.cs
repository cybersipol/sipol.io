﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.IO.Files.Interfaces
{
    public interface IFileExists
    {
        bool IsExists(string path);
    }
}
