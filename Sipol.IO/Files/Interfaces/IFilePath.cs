﻿namespace Sipol.IO.Files.Interfaces
{
    public interface IFilePath
    {
        string FilePath { get; }
    }
}
