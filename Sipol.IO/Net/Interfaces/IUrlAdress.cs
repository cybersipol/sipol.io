﻿using System;

namespace Sipol.IO.Net.Interfaces
{
    public interface IUrlAdress
    {
        Uri Url { get; }
    }
}
