﻿namespace Sipol.IO.Net.Interfaces
{
    public interface IWebDataSender
    {
        string SendWebData();
    }
}
