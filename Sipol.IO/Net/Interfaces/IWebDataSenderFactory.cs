﻿namespace Sipol.IO.Net.Interfaces
{
    public interface IWebDataSenderFactory
    {
        IWebDataSender MakeWebDataSender();
    }
}
