﻿namespace Sipol.IO.Net.Interfaces
{
    public interface IWebResultInterpreter
    {
        void InterpreteResult();
    }
}
