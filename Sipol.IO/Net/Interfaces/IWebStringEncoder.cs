﻿using System.Text;

namespace Sipol.IO.Net.Interfaces
{
    public interface IWebStringEncoder
    {
        Encoding WebEncoding { get; }
    }
}
