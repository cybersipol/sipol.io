﻿namespace Sipol.IO.Net.Interfaces
{
    public interface IWebStringLoader
    {
        string DownloadString();
    }
}
